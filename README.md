This Project was inspired and based on araczkowski and fuzziebrain projects.
One Docker Container will be created with Oracle XE 18c, Tomcat 9, Ords and Apex 19.1

**Important**
Before you build the image you need to make sure to download all the Files listed at the bottom, at the moment the files must be named the same otherwise it won´t work.

*Building the Image*
You must pass some build-arg to docker so the image can build successfully:
At the moment you can only choose a version written in the brackets.

* `--build-arg PASSWORD=<password>`
* `--build-arg APEXPASS=<apex-password>`
* `--build-arg version_apex=[19.1 ; 18.2 ; 18.1]`
* `--build-arg version_oracledb=[18c ; 11gR2]`
* `--build-arg version_ords=[19.1 ; 18.4]`
* `--build-arg version_tomcat=[9]`

`docker build -t <image-name> --build-arg PASSWORD=<password> ...[build arguments] .`


*Starting the container*
`docker run -d --name <container-name> -p 46934:22 -p 8080:8080 -p 1521:1521 <image-name>`
The container needs some time to start, around ~10 min.

**Information:**
After a successfull start the ports will be bound to the services:

**Ports**
*  Tomcat: `http://<your-host>:8080/`
*  ORDS/Apex: `http://<your-host>:8080/ords`

**Services**
*  `Oracle: sid=XE; servicename=xepdb1; username=sys; password=<password>`
*  `Tomcat: username=admin; password=<password>`
*  `Apex: workspace=internal; username=admin; password=<apexpass>`

**Files:**
- apache-tomcat-9.0.19.tar.gz
- apex_19.1_en.zip
- jre-8u211-linux-x64.tar.gz
- oracle-database-xe-18c-1.0-1.x86_64.rpm
- oracle instantclient19.3-basic-19.3.0.0.0-1.x86_64.rpm
- ords-18.4.0.354.1002.zip

**Downloads:**
- [Tomcat](https://tomcat.apache.org/download-90.cgi)
- [Oracle Apex](https://www.oracle.com/technetwork/developer-tools/apex/downloads/index.html)
- [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Oracle XE](https://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html)
- [Oracle ORDS](https://www.oracle.com/technetwork/developer-tools/rest-data-services/downloads/index.html)

**Comming soon..**
* Version controls for the all the Services
* Conversion to docker-compose for multiple instances of(Apex, oracle)

**References**
- https://github.com/araczkowski/docker-oracle-apex-ords
- https://github.com/fuzziebrain/docker-oracle-xe
