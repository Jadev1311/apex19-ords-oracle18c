REM Stopping and Removing the container
docker stop apex19-test
docker container rm apex19-test

REM Removing all old Images
docker image rm -f apex19

REM Building New Image
docker build -t apex19 --build-arg PASSWORD=test1234 --build-arg APEXPASS=$uperP@ssword1 --build-arg version_oracledb=18c --build-arg version_apex=18.2 --build-arg version_ords=18.4 --build-arg version_tomcat=9 .

REM Starting container with new image
docker run -d --name apex19-test -p 49160:22 -p 8080:8080 -p 1521:1521 apex19

REM Starting Shell connected to Container
docker exec -it apex19-test /bin/sh -c "[ -e /bin/bash ] && /bin/bash || /bin/sh"
