#!/usr/bin/env bash

if [ $version_tomcat = 9 ]
then
    export tomcat_v="apache-tomcat-9.0.19.tar.gz" 
    echo "Tomcat Version: 9"
else
    export tomcat_v="apache-tomcat-9.0.19.tar.gz" 
    echo "Tomcat Default Version: 9"
fi 

if [ $version_apex = 19.1 ]
then
    export apex_v="apex_19.1_en.zip"
    echo "Apex Version: 19.1"
elif [ $version_apex = 18.2 ]
then
    export apex_v="apex_18.2_en.zip"
    echo "Apex Version: 18.2"
elif [ $version_apex = 18.1 ]
then
    export apex_v="apex_18.1_en.zip"
    echo "Apex Version: 18.1"
elif [ $version_apex = 5.1 ]
then
    export apex_v="apex_5.1.4_en.zip"
    echo "Apex Version: 5.1"
else
    export apex_v="apex_19.1_en.zip"
    echo "Apex Default Version: 19.1"
fi

if [ $version_oracledb = 18c ]
then 
    export oraclexe_v="oracle-database-xe-18c-1.0-1.x86_64.rpm"
    echo "Oracle DB Version: 18c"
elif [ $version_oracledb = 11gR2 ]
then 
    echo "Oracle DB Version: 11gR2 is still work in Progress :( Using 18c instead"
    export oraclexe_v="oracle-database-xe-18c-1.0-1.x86_64.rpm"
else
    export oraclexe_v="oracle-database-xe-18c-1.0-1.x86_64.rpm"
    echo "Oracle DB Default Version: 18c"
fi


if [ $version_ords = 19.1 ]
then
    echo "ORDS Version: 19.1"
    export ords_v="ords-19.1.0.092.1545.zip"
elif [ $version_ords = 18.4 ]
then 
    export ords_v="ords-18.4.0.354.1002.zip"
    echo "ORDS Version: 18.4"
else
    export ords_v="ords-18.4.0.354.1002.zip"
    echo "ORDs Default Version: 18.4"
fi

export -p