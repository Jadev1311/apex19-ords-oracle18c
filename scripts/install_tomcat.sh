#!/bin/bash
cd /
source /scripts/versioncontrol.sh

tar -vxzf /files/apache-tomcat-9.0.19.tar.gz
ls -l
mv -v -f apache-tomcat-9.0.19 tomcat
cd /scripts
sed -i -e 's/password="secret"/password="'$PASSWORD'"/g' /scripts/tomcat-users.xml
sed -i -e 's/password="secret"/password="'$PASSWORD'"/g' /scripts/tomcat
mv /scripts/tomcat-users.xml /tomcat/conf/
mv /scripts/tomcat /etc/init.d/tomcat/
mv -f /scripts/context.xml /tomcat/webapps/manager/META-INF/
chmod 755 /etc/init.d/tomcat
#update-rc.d tomcat defaults 80 01
