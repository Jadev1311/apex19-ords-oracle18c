alter session set container=xepdb1;
--ALTER USER SYS IDENTIFIED BY secret ACCOUNT UNLOCK;
--ALTER USER SYS IDENTIFIED BY secret ACCOUNT UNLOCK CONTAINER=ALL;

--ALTER SESSION SET CONTAINER = pdb1;
ALTER USER APEX_LISTENER IDENTIFIED BY secret ACCOUNT UNLOCK;
ALTER USER APEX_PUBLIC_USER IDENTIFIED BY secret ACCOUNT UNLOCK;
--ALTER USER APEX_REST_PUBLIC_USER IDENTIFIED BY secret ACCOUNT UNLOCK;

-- The next one will fail if you've never installed ORDS before. Ignore errors.
ALTER USER ORDS_PUBLIC_USER IDENTIFIED BY secret ACCOUNT UNLOCK;
begin
apex_util.set_security_group_id( 10 );
  apex_util.create_user(
    p_user_name => 'ADMIN',
    p_email_address => 'test@mail.de',
    p_web_password => 'apexpw',
    p_developer_privs => 'ADMIN',
    p_change_password_on_first_use => 'Y');
      apex_util.set_security_group_id( null );
  commit;
end;
/
exit