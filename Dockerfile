FROM centos:latest

ARG PASSWORD
ENV PASSWORD ${PASSWORD:-secret}
ARG APEXPASS
ENV APEXPASS ${APEXPASS}

#Versioncontrol
ARG version_oracledb
ENV version_oracledb ${version_oracledb}
ARG version_apex
ENV version_apex ${version_apex}
ARG version_ords 
ENV version_ords ${version_ords}
ARG version_tomcat
ENV version_tomcat=${version_tomcat}

ENV ORACLE_HOME /opt/oracle/product/18c/dbhomeXE
ENV PATH $ORACLE_HOME/bin:$PATH
ENV ORACLE_SID=XE
ENV ORACLE_DOCKER_INSTALL=true

EXPOSE 22 1521 8080

COPY scripts /scripts

COPY files /files

RUN bash /scripts/versioncontrol.sh && source /scripts/versioncontrol.sh
RUN bin/bash -c ". /scripts/install_main.sh"

ADD entrypoint.sh /
ENTRYPOINT [ "/entrypoint.sh" ]